// console.log("Hello World");

// ES6 is also known as ECMAScript 2015
// ECMAScript is the standard that is used to create implementation of the language, one which is Javascript.
    // where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.)
// New features to JavaScript

// [SECTION] Exponent Operator

// Using Math objects methods
const firstNum = Math.pow(8, 2,)
console.log(firstNum);

// Using the exponent operator
const secondNum = 8 ** 2;
console.log(secondNum);


// [SECTION] Template Literals
/* 
    - Allows to write string without using the concatenation operator (+).
    - ${} is called placeholder when using template literals, and we can input variables or expression.

*/
let name = "John";

// Hello John! Welcome to Programming!
// Pre-Template Literals
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals:");
console.log(message);

// Strings using template literals
// Uses backticks (``) instead of ("") or ('')
message = `Hello ${name}! Welcome to programming!`;
console.log("Message with template literals:");
console.log(message);

const anotherMessage = `${name} attended a math competition. 
He won it by solving the problem "8 ** 2" with the solution of ${8**2}.`;
console.log(anotherMessage);


// [SECTION] Array Destructuring
// It allows us to name array elements with variable names instead of using the index numbers.
/* 
    - Syntax:
        let/const [variableName1, variableName2, variableName3] = arrayName;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Hello Juan Dela Cruz! It's nice to meet you.
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
// 
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

//only the last variable will be read
// console.log(`Hello ${firstName, middleName, lastName}! It's nice to meet you!`);



// [SECTION] Object Destructuring
/* 
    - Shortens the syntax for accessing properties from object
    - The difference with array destructuring, this should be exact property name.
    - Syntax:
        let/const {propertyNameA, propertyB, propertyC} = object;

*/

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    surName: "Cruz"
};

// Pre-object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.surName}! It's good to see you again.`);

// Object Destructuring
// const {givenName, maidenName, surName} = person;
// console.log(givenName);
// console.log(maidenName);
// console.log(surName);

// console.log(`Hello ${givenName} ${maidenName} ${lastName}! It's good to see you again.`);

                    // Object destructuring can also be done inside the parameter.
function getFullName({givenName, maidenName, surName}){
    console.log(`Hello ${givenName} ${maidenName} ${lastName}!`);
}

getFullName(person);


// [SECTION] Arrow Functions
/* 
    - Compact alternative syntax to traditional functions.
    - Useful for code snippets when creating functions will not be reused in any other portion of the code.
    - This will only work with "function expression" -> contained in a specific variable.
    - "DRY (Don't Repeat Yourself)" principle
    - Syntax:
        let/const variableName = () =>{
            //statement/code block
        }

        let/const variableName = (parameter) =>{
            //statement with parameter/code block
        }
*/
    // hello();
    // function declaration (hoisting)
    // works here since it is function declaration so invoking can be done before declaration

    // function hello(){
    //     console.log("Hello World!");
    //   }

    // works as function expression so invoking should be done after initialization/declaration
      const hello = () =>{
        console.log("Hello World");
      }

      hello();


      const students = ["John", "Jane", "Judy"];

    //   Arrow functioncs using Array Iteration method
    // Pre-Arrow Function
    // students.forEach(function(student){
    //     console.log(`${student} is a student.`);
    // });

    // Arrow Function
    students.forEach((student) => {
        console.log(`${student} is a student.`)
    });


// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement

// const add = (x, y) =>{
//     return x+ y;
// }

// let total = add(1, 2);
// console.log(total);


// Using implicit return
const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);

const numbers = [1, 2, 3, 4, 5];

const squareValues = numbers.map((number) => number ** 2)
console.log(squareValues);


// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined


const greet = (name = "User") => `Good morning, ${name}`;
console.log(greet());
console.log(greet("John"));



// [SECTION] Class-Based Object Blueprint
// Allows creation/instantiation of object using classes as blueprints.
/* 
    - The 'constructor' is a special method of a class for creating/initializing object for that class.
    - Syntax:
        class className{
            constructor(objectPropertyA, objectPropertyB){
                this.objectPropertyA = objectPropertyA;
                this.objectPropertyB = objectPropertyB;
            }
            // insert function outside our constructor
        }
*/
    // function constructor
    // function Car(brand, name, year){
    //     this.brand = brand;
    //     this.name = name;
    //     this.year = year;
    // }

    class Car{
        constructor(brand, name, year){
            this.brand = brand;
            this.name = name;
            this.year = year;
        }
    }

    // "new" operator creates/instantiates a new object with the given argument as value of its property.
    const myCar = new Car();
    console.log(myCar); //we have created a object but its properties are undefined

    // Reassign value of each properties
    myCar.brand = "Ford";
    myCar.name = "Ranger Raptor";
    myCar.year = 2021;

    console.log(myCar);